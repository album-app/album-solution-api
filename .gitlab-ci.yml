# CI Routine for the album-solution-api framework.


#----------------------------
# templates
#----------------------------

# Linux base template
#
# Uses a docker image where conda is already installed.
# Creates an album environment.
#
.linux_base_template:
  image: python:latest
  tags:
    - docker
    - linux
  before_script:
    - python -V  # Print out python version for debugging
    - which python
    - pwd
    - pip install .
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
  cache:
    key: one-key-to-rule-them-all-linux
    paths:
      - .cache/pip

# Macos base template
#
# uses the shell and the python available
#
.macos_base_template:
  tags:
    - macos
    - shell
  variables:
    MICROMAMBA_EXECUTABLE: "" # ensure that the variable is empty before running the job
    PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
  before_script:
    - echo "$(uname)"
    - sw_vers
    - tmpdir=$(mktemp -d /tmp/album-test.XXXXXX)
    - echo $tmpdir
    - echo $tmpdir > /tmp/tmpdir
    - mkdir -p $tmpdir/bin/
    - curl -Ls https://micro.mamba.pm/api/micromamba/osx-64/1.5.6 | tar -xvj -C $tmpdir/bin/ --strip-components=1 bin/micromamba
    - MICROMAMBA_EXECUTABLE=$tmpdir/bin/micromamba
    - $MICROMAMBA_EXECUTABLE create -y -n album-solution-api python=3.12 -c conda-forge
    - bash -c 'echo $CI_COMMIT_REF_NAME'
    - $MICROMAMBA_EXECUTABLE run -n album-solution-api pip install --no-cache-dir -e .
    - $MICROMAMBA_EXECUTABLE run -n album-solution-api python -V
    - $MICROMAMBA_EXECUTABLE run -n album-solution-api pip list
    - $MICROMAMBA_EXECUTABLE run -n album-solution-api env -0 | sort -z | tr '\0' '\n' | grep -i album
  after_script:
    - tmpdir=$(</tmp/tmpdir)
    - echo $tmpdir
    - rm -rf $tmpdir
# Windows base template
#
# Downloads conda executable in <current_working_directory>\downloads if executable not present.
# Installs conda in >current_working_directory>\miniconda if conda not already installed.
# Runs conda initialization and configuration.
#
# NOTE: Apparently there is no output for commands split over several lines...
.windows_base_template:
  before_script:
    - 'echo "We are in path: $pwd "'
    - 'Write-Output "CI_COMMIT_REF_NAME: $env:CI_COMMIT_REF_NAME"'
    - '$7ZIP_EXE=(Join-Path $env:ProgramFiles -ChildPath 7-Zip\7z.exe)'
    - "$7ZIP_INSTALLER=(Join-Path downloads -ChildPath 7z.exe)"
    - "$Env:USERPROFILE=$pwd" # set user profile to current working directory -> will be micromamba default location
    - '$oldProgressPreference = $progressPreference; $progressPreference = "SilentlyContinue";'
    - "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12" # set Security download protocol to avoid https errors
    - "if(-Not (Test-Path $7ZIP_EXE)) {if(-Not (Test-Path $7ZIP_INSTALLER)){Invoke-Webrequest -URI https://www.7-zip.org/a/7z2201-x64.exe -OutFile $7ZIP_INSTALLER}}"
    - 'if(-Not (Test-Path $7ZIP_EXE)) {Start-Process -FilePath $7ZIP_INSTALLER -Args "/S" -Verb RunAs -Wait}'
    - "Invoke-Webrequest -URI https://micro.mamba.pm/api/micromamba/win-64/1.5.6 -OutFile micromamba.tar.bz2"
    - "& $7ZIP_EXE x micromamba.tar.bz2 -aoa"
    - '& $7ZIP_EXE x micromamba.tar -ttar -aoa -r Library\bin\micromamba.exe $("-o" + (Join-Path -Path (Get-Location) -ChildPath micromamba_installation))'
    - "$progressPreference = $oldProgressPreference"
    - 'Get-ChildItem Env: | Where-Object { $_.Name -match "album" -or $_.Value -match "album" } | Format-Table -AutoSize'
    - '.\micromamba_installation\Library\bin\micromamba.exe create -y -n album-solution-api -c conda-forge python=3.12' # created in .\micromamba\envs\album
    - '.\micromamba_installation\Library\bin\micromamba.exe run -n album-solution-api pip install -e .'
  tags:
    - windows
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR\.cache\pip
    ErrorActionPreference: Continue  # not working properly
  cache:
    key: one-key-to-rule-them-all-windows
    paths:
      - .cache\pip

#----------------------------
# stages
#----------------------------

stages:
  - test
  - release
  - deploy

#----------------------------
# jobs
#----------------------------

prepare release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  tags:
    - DeployRunner
    - DeployDocker
  script:
    - echo "preparing release"
    - version=$(cat setup.cfg | grep "version=*" | awk '{split($0, s, " "); print s[3]}')
    - TAG=v$version
    - echo "prepare to release version $version with tag $TAG"
    - echo "VERSION=$version" >> variables.env
    - echo "TAG=v$version" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

gitlab release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  tags:
    - DeployRunner
    - DeployDocker
  needs:
    - job: prepare release
      artifacts: true
  script:
    - echo "running release_job for version $version and tag $TAG"
  artifacts:
    reports:
      dotenv: variables.env
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  release:
    name: 'Release $TAG'
    description: 'Created using the release-cli'
    tag_name: '$TAG'
    ref: '$CI_COMMIT_SHA'


gitlab pypi deploy:
  image: python:latest
  stage: deploy
  tags:
    - DeployRunner
    - DeployDocker
  needs:
    - job: gitlab release
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - echo "deploying gitlab pypi version "$VERSION
    - pip install twine setuptools wheel
    - python setup.py sdist bdist_wheel
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

pypi deploy:
  image: python:latest
  stage: deploy
  tags:
    - DeployRunner
    - DeployDocker
  needs:
    - job: gitlab release
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - echo "deploying pypi version "$VERSION
    - echo "using pypi deploy user "$PYPI_DEPLOY_USER
  script:
    - pip install twine setuptools wheel
    - python setup.py sdist bdist_wheel
    - TWINE_PASSWORD=$PYPI_DEPLOY_TOKEN TWINE_USERNAME=$PYPI_DEPLOY_USER python -m twine upload --repository-url https://upload.pypi.org/legacy/  dist/*

unit tests linux:
  extends: .linux_base_template
  stage: test
  script:
    - python -m unittest tests/run_all.py

unit tests macos:
  extends: .macos_base_template
  stage: test
  script:
    - $MICROMAMBA_EXECUTABLE run -n album-solution-api python -m unittest tests/run_all.py

unit tests windows:
  extends: .windows_base_template
  stage: test
  script:
    - '.\micromamba_installation\Library\bin\micromamba.exe run -n album-solution-api python -m unittest tests\run_all.py '
    - "exit($lastexitcode)"